# Darts IDP

Project for an interactive darts scoreboard.
Automatically calculates (a) if a finish is possible, and (b) how to finish.
In case an error is made, it re-calculates if finishing is still possible.
Connects to IDP-Z3 via a websocket.
